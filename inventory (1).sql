-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 03, 2023 at 01:01 PM
-- Server version: 10.4.27-MariaDB
-- PHP Version: 8.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `inventory`
--

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `customers_id` smallint(5) UNSIGNED ZEROFILL NOT NULL,
  `customers_code` int(20) UNSIGNED NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(20) NOT NULL,
  `middle_name` varchar(20) NOT NULL,
  `address` varchar(100) DEFAULT NULL,
  `contact_number` bigint(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`customers_id`, `customers_code`, `first_name`, `last_name`, `middle_name`, `address`, `contact_number`) VALUES
(00001, 1, 'Abraham', 'Banzuela', 'Largo', 'blk 1 lot 9 golden homes subdv. sabang', 9202731418);

-- --------------------------------------------------------

--
-- Table structure for table `physical_inventory`
--

CREATE TABLE `physical_inventory` (
  `physical_id` tinyint(4) NOT NULL,
  `inventory_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `physical_inventory`
--

INSERT INTO `physical_inventory` (`physical_id`, `inventory_date`) VALUES
(38, '2023-05-07'),
(39, '2023-06-07'),
(40, '2023-06-06');

-- --------------------------------------------------------

--
-- Table structure for table `physical_inventory_details`
--

CREATE TABLE `physical_inventory_details` (
  `physical_id` tinyint(4) DEFAULT NULL,
  `product_id` smallint(5) UNSIGNED DEFAULT 0,
  `quantity` decimal(8,2) UNSIGNED DEFAULT NULL,
  `cost` decimal(8,2) UNSIGNED DEFAULT 0.00,
  `details_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `physical_inventory_details`
--

INSERT INTO `physical_inventory_details` (`physical_id`, `product_id`, `quantity`, `cost`, `details_id`) VALUES
(38, 6, '111.00', '10.00', 203),
(38, 4, '15.00', '120.00', 204),
(39, 5, '21.00', '12.00', 205),
(40, 8, '5.00', '5000.00', 206),
(40, 10, '25.00', '10000.00', 207);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `product_id` smallint(6) NOT NULL,
  `product_code` varchar(50) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  `price` decimal(8,2) UNSIGNED DEFAULT 0.00
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`product_id`, `product_code`, `description`, `price`) VALUES
(7, '1', 'oppo reno 8t', '5000.00'),
(8, '2', 'oppo a78 5g', '10000.00'),
(9, '3', 'oppo find x5', '12000.00'),
(10, '4', 'OPPO Reno8 Pro 5G', '15000.00');

-- --------------------------------------------------------

--
-- Table structure for table `purchases`
--

CREATE TABLE `purchases` (
  `purchase_id` mediumint(8) UNSIGNED NOT NULL,
  `invoice_number` varchar(10) DEFAULT NULL,
  `invoice_date` date DEFAULT NULL,
  `supplier_id` mediumint(8) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `purchases`
--

INSERT INTO `purchases` (`purchase_id`, `invoice_number`, `invoice_date`, `supplier_id`) VALUES
(1, '1', '2023-05-15', 1),
(3, '2', '2023-06-06', 1);

-- --------------------------------------------------------

--
-- Table structure for table `purchases_details`
--

CREATE TABLE `purchases_details` (
  `pd_id` mediumint(8) UNSIGNED NOT NULL,
  `purchase_id` mediumint(8) UNSIGNED DEFAULT NULL,
  `product_id` mediumint(8) UNSIGNED DEFAULT NULL,
  `quantity` decimal(8,2) DEFAULT NULL,
  `price` decimal(8,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `purchases_details`
--

INSERT INTO `purchases_details` (`pd_id`, `purchase_id`, `product_id`, `quantity`, `price`) VALUES
(4, 1, 5, '4.00', '700.00'),
(5, 1, 2, '50.00', '60.00'),
(6, 1, 1, '50.00', '50.00'),
(7, 3, 5, '1.00', '10.00');

-- --------------------------------------------------------

--
-- Table structure for table `sales`
--

CREATE TABLE `sales` (
  `sale_id` mediumint(8) UNSIGNED NOT NULL,
  `invoice_number` varchar(10) DEFAULT NULL,
  `invoice_date` date DEFAULT NULL,
  `customers_id` smallint(5) UNSIGNED DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `sales`
--

INSERT INTO `sales` (`sale_id`, `invoice_number`, `invoice_date`, `customers_id`) VALUES
(59, '1', '2023-05-06', 3),
(61, '2', '2023-05-07', 4),
(64, '3', '2023-06-21', 0),
(65, '5', '2023-01-10', 0);

-- --------------------------------------------------------

--
-- Table structure for table `sale_details`
--

CREATE TABLE `sale_details` (
  `sale_id` mediumint(8) UNSIGNED DEFAULT 0,
  `product_id` smallint(5) UNSIGNED DEFAULT 0,
  `quantity` decimal(8,2) UNSIGNED DEFAULT 0.00,
  `price` decimal(8,2) UNSIGNED DEFAULT 0.00,
  `sd_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `sale_details`
--

INSERT INTO `sale_details` (`sale_id`, `product_id`, `quantity`, `price`, `sd_id`) VALUES
(59, 4, '11.00', '8.00', 22),
(59, 5, '15.00', '100.00', 23),
(61, 1, '15.00', '510.00', 24),
(61, 3, '110.00', '125.00', 25),
(61, 2, '5.00', '60.00', 26),
(64, 5, '1.00', '10.00', 27),
(65, 5, '1.00', '10.00', 28);

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `supplier_id` smallint(5) UNSIGNED NOT NULL,
  `supplier_code` varchar(30) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `contact` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`supplier_id`, `supplier_code`, `name`, `address`, `contact`) VALUES
(2, '2', 'Oppo Company', 'Taguig,Manila', '8852 0194');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `login_id` int(10) UNSIGNED NOT NULL,
  `names` varchar(50) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `user_type` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`login_id`, `names`, `username`, `password`, `user_type`) VALUES
(1, 'Abraham L. Banzuela', 'admin', '12345', 'admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`customers_id`) USING BTREE;

--
-- Indexes for table `physical_inventory`
--
ALTER TABLE `physical_inventory`
  ADD PRIMARY KEY (`physical_id`);

--
-- Indexes for table `physical_inventory_details`
--
ALTER TABLE `physical_inventory_details`
  ADD PRIMARY KEY (`details_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`product_id`),
  ADD UNIQUE KEY `description` (`description`);

--
-- Indexes for table `purchases`
--
ALTER TABLE `purchases`
  ADD PRIMARY KEY (`purchase_id`),
  ADD UNIQUE KEY `invoice_number` (`invoice_number`) USING BTREE;

--
-- Indexes for table `purchases_details`
--
ALTER TABLE `purchases_details`
  ADD PRIMARY KEY (`pd_id`);

--
-- Indexes for table `sales`
--
ALTER TABLE `sales`
  ADD PRIMARY KEY (`sale_id`),
  ADD UNIQUE KEY `invoice_number` (`invoice_number`),
  ADD KEY `invoice_date` (`invoice_date`),
  ADD KEY `customers_id` (`customers_id`) USING BTREE;

--
-- Indexes for table `sale_details`
--
ALTER TABLE `sale_details`
  ADD PRIMARY KEY (`sd_id`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`supplier_id`),
  ADD UNIQUE KEY `supplier_code` (`supplier_code`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`login_id`),
  ADD UNIQUE KEY `name` (`names`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `customers_id` smallint(5) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `physical_inventory`
--
ALTER TABLE `physical_inventory`
  MODIFY `physical_id` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `physical_inventory_details`
--
ALTER TABLE `physical_inventory_details`
  MODIFY `details_id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=208;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `product_id` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `purchases`
--
ALTER TABLE `purchases`
  MODIFY `purchase_id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `purchases_details`
--
ALTER TABLE `purchases_details`
  MODIFY `pd_id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `sales`
--
ALTER TABLE `sales`
  MODIFY `sale_id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT for table `sale_details`
--
ALTER TABLE `sale_details`
  MODIFY `sd_id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `supplier`
--
ALTER TABLE `supplier`
  MODIFY `supplier_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `login_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
